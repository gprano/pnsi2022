# Algorithmes de tri

## Tri par sélection

Principe : on place les éléments directement au bon endroit, du plus petit au plus grand.
À chaque étape on **sélectionne** le minimum dans les éléments restants, et on le place au début
des éléments restants.

On a besoin de savoir trouver la position du minimum dans la liste à partir d'une certaine
position, et de savoir échanger les éléments à deux positions données de la liste, ce qu'on
fait dans des fonctions à part. Cela permet de découper le code en trois parties plus simples.

```python
def position_minimum(l, depart):
	"""renvoie la position du minimum dans la liste l à partir de la position depart"""
	n = len(l)
	imin = depart
	for i in range(depart,n):
		if l[i] < l[imin]:
			imin = i
	return imin
```

```python
def echange(l,a,b):
	"""échange les éléments en positions a et b dans la liste l"""
	sauvegarde_a = l[a]
	l[a] = l[b]
	l[b] = sauvegarde_a
```

```python
def tri_selection(l):
	"""trie la liste l (la liste est modifiée en place, cette fonction ne renvoie rien)"""
    n = len(l)
    for i in range(n):
        imin = position_minimum(l, i)
		echange(l, imin, i)
```


## Tri par insertion

Principe : On prend chaque élément de la liste, et on l'**insère** à sa place
dans la partie à sa gauche qui est déjà rangée dans l'ordre (ce ne sont pas
forcément les plus petits éléments comme dans le tri par sélection, mais ils
sont bien ordonnés). Pour le mettre à sa place, on l'échange avec son voisin de
gauche tant que celui-ci est plus grand et qu'on n'est pas arrivé tout à gauche.

```python
def tri_insertion(l):
	"""trie la liste l (la liste est modifiée en place, cette fonction ne renvoie rien)"""
    n = len(l)
    for i in range(n):
        j = i
        while j>0 and l[j-1]>l[j]:
			echange(l,j,j-1)
            j -= 1
```
