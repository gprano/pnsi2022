# Derniers ajouts

* [Introduction](web/javascript.md) au langage javascript et [mini-projet](web/projets_js.md).

* [Projet](projets/projet_bibli.md) de création de bibliothèque

* [Parcours de listes et dictionnaires](python/parcours.md) (rappels)

* [Traitement de données](donnees/tp_csv.md) au format csv en python.

* Dictionnaires sur [futurecoder](https://futurecoder.forge.aeif.fr/course/#IntroducingDictionaries) (plus simple) ou avec un [tp d'annuaire avec flask](https://nsi.codeberg.page/premiere/repr-cons/dict/) (plus dur). Puis [introduction au traitement de données en tables](https://clogique.fr/nsi/premiere/donnees_table/).

* [TP](http://frederic-junier.org/NSI/premiere/chapitre25/tp/TP-Filius-NSI-2020V1.pdf) de simulation de réseau avec le logiciel Filius (fichiers pour l'[exercice 4](os/exercice4_ressources.fls) et l'[exercice 5](os/exo5.fls))

* [TP](os/tp_flask.md) pour faire un serveur web _dynamique_ avec python

* Programmation en assembleur :
    * [exercices](https://qkzk.xyz/docs/nsi/cours_premiere/architecture/5_aqa/5_assembleur_td/)
    * utilisation du simulateur AQA : [partie 1](https://qkzk.xyz/docs/nsi/cours_premiere/architecture/5_aqa/6_assembleur_aqa_intro/) et [partie 2](https://qkzk.xyz/docs/nsi/cours_premiere/architecture/5_aqa/7_assembleur_aqa_tp/)

* [Corrigé](corrige.md) du premier devoir de 2h.

* Circuits logiques :
    * [portes de base](http://nsi.janviercommelemois.fr/fichiers_circuits/circuits-nand.html) avec NAND.
    * [exercices](http://nsi.janviercommelemois.fr/fichiers_circuits/circuits-exercices-feuille.html) supplémentaires.
    * [Unité Arithmétique et Logique](http://nsi.janviercommelemois.fr/fichiers_circuits/circuits-pour-ual1.html)
    * [Ajouter de la mémoire](circuits/memoire.md) aux circuits logiques

* [Projet](projets/projet2D.md) de jeu dans une grille en 2D.

* [Vérifier et distribuer son site web](web/partie2.md)

* [HTML/CSS](web/html_css.md) : créer un site web de base

* [Exposé](expose_commande.md) à préparer pendant les vacances d'automne.

* [TP](os/tp1.md) ligne de commande et réseau