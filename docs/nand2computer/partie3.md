# Partie 3: Mémoire

Avec la porte Nand, on a créé des circuits pour faire des calculs sur des nombres,
mais tous ces circuits font (presque) **instantanément** le calcul du résultat sur
leurs fils de sortie en fonction du courant électrique sur leurs fils d'entrée.

Si on change les valeurs en entrée, les valeurs en sortie changent automatiquement.
En conséquence, ces circuits ne pourront jamais avoir de mémoire.

On introduit donc une nouvelle puce électronique de base, qu'on suppose qu'on nous
donne en grande quantité, et dont on va se servir, le **DFF (data flip flop)** :

* En entrée : in
* En sortie : out a la valeur de in **au temps précédent** : out(t) = in(t-1)

![DFF](dff.png)

Cette porte est reliée à une horloge qui est dans tous les ordinateurs, et
qui envoie un tic régulièrement, quelques milliards de fois par seconde
pour les ordinateurs actuels. La fréquence en GHz que vous pouvez lire dans
la description d'un processeur correspond au nombre de milliards de tic par seconde.

Un DFF ne fait que décaler dans le temps, d'un tic, les valeurs qu'il reçoit en entrée.

Grâce à ça, on va pouvoir fabriquer de la mémoire.

### Bit

* En entrée : in et load
* En sortie : out renvoie la valeur stockée, et si load vaut 1
  change la valeur pour la mettre à in. Donc out(t) = out(t-1) (rien ne change)
  sauf si load(t-1) = 1 et alors out(t) = in(t-1).

![Bit](bit.png)

??? Indice
	Pour sauvegarder quelque chose, il faut rebrancher la sortie d'un DFF
	sur son entrée. Mais on ne peut pas le faire directement parce qu'il y a
	déjà l'entrée in. Il faut donc mettre un Mux pour choisir entre l'entrée in
	et la sortie du DFF, en fonction de load qui sert de sélecteur.
	
??? Solution
	![Bit_solution](bit_impl.png)

### Register

* En entrée : in[16] et load
* En sortie : out[16]

![Register](register.png)

Un registre est une version 16-bits de Bit, qui stocke 16 bits d'un coup.
Il se construit comme les autres portes à 16-bits qu'on a déjà vues (on aurait
pu l'appeler Bit16 au lieu de Register).

### RAM8, RAM64, ...

RAM**n** est un circuit qui possède **n** registres et qui prend un entrée
une adresse (un nombre en binaire de 0 à n-1) pour savoir sur quel registre
on veut opérer. Ensuite l'opération est comme pour un registre, si load vaut 1
on change la valeur du registre avec in et on renvoie la valeur précédente en
sortie. Les autres registres ne changent pas.

* En entrée : in[16], load, adress[nombre de bits nécessaires pour aller de 0 à n-1]
* En sortie : out[16]

![RamN](ram.png)

??? Indice
	Chaque circuit de RAM est créé avec 8 circuits de RAM plus petites.
	![RAM_sol](ram_impl.png)
	
	Il y a deux parties logiques à rajouter : 
	
    * DMux8Way pour envoyer load sur le load du registre correspondant à l'adresse
	* Mux8Way16 pour envoyer sur out le out du registre correspondant à l'adresse

### PC (compteur)

Le PC (program counter) va servir à retenir le numéro de l'instruction du programme
qu'on est en train d'exécuter sur l'ordinateur.

* En entrée : 
    * in[16] : un nouveau numéro d'instruction
	* inc : si égal à 1 on augmente de 1 la valeur enregistrée (sert à passer
	à l'instruction suivante)
	* load : si égal à 1 on change la valeur et on met celle de in (sert à faire
	un saut dans le programme, par exemple à cause d'une boucle)
	* reset : si égal à 1 on met 0 (sert à revenir au début du programme)
* En sortie : out[16]


![PC](pc.png)
