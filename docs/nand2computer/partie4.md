# Partie 4: Ordinateur

## Architecture de Von Neumann

![Von Neumann Architecture](vonneumann.png)

## Mémoire

Mémoire en lecture seule (Read Only Memory) qui sert
à stocker le programme à exécuter.

![ROM](rom.png)

Mémoire pour :

* les données utilisées par le programme (RAM)
* l'écran, qui est vu comme une zone de mémoire où ce qu'on
écrit est affiché sur l'écran physique.
* le clavier, qui est vu comme un unique registre où on
peut lire le code de la touche qui est enfoncée.

![Mémoire](memory.png)

## CPU (Processeur)

![CPU](cpu.png)

Le CPU est composé de :

* l'ALU, pour les calculs
* Trois registres :
    * Un registre D qui sert à stocker les données
	* Un registre A, qui sert à stocker l'adresse de
	la mémoire à laquelle on va lire ou écrire
	* Le compteur PC qui stocke et calcule l'adresse
	de l'instruction suivante à exécuter
* la partie contrôle, qui sert à décoder l'instruction
pour l'exécuter correctement avec l'ALU et les registres,
et renvoyer les bonnes sorties.

??? Implémentation
	![Implémentation du CPU](cpu_impl.png)
	
	Les **c** sur les schéma sont les bits de contrôle, qui
	doivent être calculés par la partie contrôle (non représentée)
	en fonction de l'instruction.
	
## Computer

L'ordinateur complet !

![Computer](computer.png)

??? Implémentation
	![Implémentation de Computer](computer_impl.png)

