## Représentation des nombres en binaire

On a vu que les fils électrique dans les ordinateurs n'ont que deux valeurs de tension
possibles, qu'on représente par 0 et 1. Pour pouvoir manipuler des nombres, on va considérer
une série de fils, 16 dans notre projet, qui vont représenter les chiffres d'un nombre
qui est donc écrit en binaire, avec des 0 et des 1.


!!! info "Principe"
	Le binaire fonctionne exactement comme la base 10, sauf qu'au lieu d'avoir les
	chiffres de 0 à 9, on a des chiffres de 0 à 1.  
	Les opérations (compter, additionner...) que vous savez faire en base 10 fonctionnent aussi en binaire.

Si on reprend ce que vous savez déjà faire en maths :

* **Compter :** Pour compter à partir de 0, on ajoute à chaque fois 1 au chiffre de droite.
  Si ce chiffre est déjà le chiffre maximum (9 en base 10, 1 en binaire), on le remet à 0 et
  on ajoute 1 au chiffre suivant (la retenue).
  
* **Additioner deux nombres :** On peut poser l'addition de deux nombres à plusieurs chiffres
  comme vous avez appris à le faire en primaire. 1+1 donne 0 avec une retenue sur le chiffre
  suivant. C'est cet algorithme que vous avez appris en primaire dont on va se servir pour
  faire un circuit qui fait l'addition.
  
  ![addition posée en binaire](addition.png){: align=center }
  
* **Nombres spéciaux :**
    * En base 10, un 1 suivi de n zéros représente le nombre $10^n$  
      En binaire, un 1 suivi de n zéros représente le nombre $2^n$.
	* En base 10, un nombre composé de $n$ chiffres 9 représente $10^n-1$.  
	  En binaire, un nombre composé de $n$ chiffres 1 représente $2^n-1$.
    * En base 10, les nombres dont le dernier chiffre est 0 sont multiples de 10.  
	  En binaire, les nombres dont le dernier chiffre est 0 sont multiples de 2 (pairs).

## Conversion entre les bases

**Binaire vers décimal:** 

* En base 10 on a $943 = 9*10^2 + 4*10^1 + 3*10^0$.
* De même en binaire $1011_b = 1*2^3 + 0*2^2 + 1*2^1 + 1*2^0$  
  Soit $1011_b = 2^3 + 2^1 + 2^0 = 8 + 2 + 1 = 11$
  
**Décimal vers binaire:**

On peut déduire l'écriture binaire avec l'algorithme suivant, qui trouve les
chiffres du plus petit au plus grand.

```
Tant que le nombre n'est pas zéro:
	Si le nombre est impair:
		Rajouter le chiffre 1 et enlever 1 au nombre.
	Sinon:
		Rajouter le chiffre 0.
	Diviser le nombre par deux.
```

!!! info "Avec python"
	La fonction bin renvoie la représentation binaire d'un entier sous forme de texte avec "0b" comme préfixe :
	```python
	>>> bin(42)
	'0b101010'
	```
	$101010_b$ est l'écriture binaire de 42.
	

	Pour écrire un nombre (de type int) en binaire en python, on écrit 0b comme préfixe :
	```python
	>>> 0b11001
	25
	```
	25 est l'écriture décimale de $11001_b$. Python affiche toujours les nombres en base 10.

## Notation des opérations

Vous pouvez rencontrer plusieurs notations pour les opérations logiques en binaire
dont on a fait les circuits, voici les principales :

| logique | mathématique | en python |
| --- | --- | --- |
| not x | $\bar{x}$ | ~x |
| x and y | $x \land y$ | x & y |
| x or y | $x \lor y$ | x \| y |


## Nombres négatifs : le complément à deux sur 16-bits

On va utiliser le fait qu'on ignore les chiffres qui dépassent 16-bits quand on
fait une addition.

Comment représenter le nombre -1 ? Ce nombre vérifie $-1 + 1 = 0$.

Or sur 16-bits, $1111111111111111 + 1 = 0$ (on devrait obtenir un 1 suivi de 
16 zéros, $2^{16}$, mais on ignore le 1 qui dépasse la taille de nos entiers).
Donc -1 sera représenté par $1111111111111111$, c'est à dire $2^{16}-1$.

Si $x$ est un nombre sur 16-bits, comment représenter $-x$ ?

De la même façon, il faudrait que $x + -x = 2^{16}$ donc on prend $-x = 2^{16} -x$.

??? "Lien entre négation binaire et complément à deux"
	On peut observer que si on additionne x avec sa négation binaire, on n'obtient que des 1.
	
	Par exemple $1111001011011011 + 0000110100100100 = 1111111111111111$

	Donc $x + \bar{x} = 2^{16}-1$

	Donc $-x = \bar{x} + 1$ et $\bar{x} = -x - 1$

