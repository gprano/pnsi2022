# Vérifier et distribuer son site web

## Vérification

Dans les langages de programmation comme python, le programme est volontairement arrêté dès qu'une erreur est rencontrée dans le code.

Dans les langages de présentation comme HTML/CSS, les navigateurs web font en général le choix d'ignorer les erreurs et d'afficher la page le mieux possible. Il est cependant important de respecter les standards du web pour que votre page soit :

* affichée de la même manière par tous les navigateurs
* accessible aux personnes qui utilisent un lecteur d'écran
* correctement indexée par les moteurs de recherche et autres outils qui traiteront la page automatiquement

**Valider le code HTML :** [Validateur HTML du W3C](https://validator.w3.org/#validate_by_upload)

**Valider le code CSS:** [Validateur CSS du W3C](https://jigsaw.w3.org/css-validator/#validate_by_upload)

Vérifiez votre code HTML/CSS, corrigez le si nécessaire et mettez-le à jour sur le nextcloud.

<!-- stack of protocols in the tcp/ip stack ? then the web protocols ? (un peu de cours)-->

## Distribuer le site web (protocole HTTP)

Ouvrez un terminal dans le dossier qui contient votre site web (soit avec clic-droit et ouvrir un terminal ici dans l'explorateur, soit ouvrir le terminal et s'y déplacer avec `cd`).

Vous pouvez distribuer votre site web sur le réseau avec la commande :

```bash
python3 -m http.server
```

Tant que cette commande est lancée et que vous ne l'arrêtez pas (avec Ctrl+C), elle va distribuer le contenu du dossier sur le réseau si elle reçoit des requêtes.

!!! info "Les ports internet"

    Vous voyez dans la réponse qu'un numéro de port est spécifié. Les ports des protocoles TCP et UDP qui gèrent l'acheminement des paquets sur le réseau permettent que plusieurs logiciels utilisent internet sur le même ordinateur. Chaque application reçoit ses données sur un port qui lui est propre. 
    
    En général, pour un serveur web on utilise le port 80 (pour le protocole HTTP qui sert à demander des pages web) ou 443 (pour HTTPS). Cependant sur linux il faut les droits root pour écouter sur ces ports, donc votre commande a dû utiliser le port 8080 ou quelque chose comme cela.

    Lorsque vous tapez une adresse dans la barre d'adresse du navigateur, vous pouvez ajouter `:8080` à la fin pour faire la requête sur le port 8080 au lieu du port habituel (80 ou 443).

Maintenant, vous pouvez accéder aux sites des autres élèves de la salle qui ont fait la même manipulation en tapant dans la barre d'adresse leur adresse IP, avec le bon numéro de port.

!!! warning "Remarque"
    Pour que la page soit affichée directement sans avoir à cliquer sur le fichier dans une liste, il faut qu'elle s'appelle index.html.

Vous remarquez aussi que dans le terminal où vous avez lancé la commande, vous pouvez voir chacune des requêtes et quelle adresse IP l'a faite. Un serveur web peut toujours connaître les adresses IP des personnes qui l'ont visité, car il faut bien envoyer les fichiers du site à cette adresse.

Côté client (dans le navigateur), si vous voulez savoir les requêtes que vous avez faites vous pouvez ouvrir la console de développement avec F12 et aller dans l'onglet "Réseau"/"Network". Vous pouvez avoir le texte exact de la requête (_raw header_), le code de réponse (200 si OK, 404 si ressource inexistante par exemple), le temps pris par la requête...

Il n'est pas vraiment possible d'utiliser HTTPS ici car ce protocole (qui chiffre les données pour les rendre illisibles par un intermédiaire entre le client et le serveur), vérifie aussi que le serveur est bien celui qui est responsable du nom de domaine (pour rendre impossible à un intermédiaire de se faire passer pour wikipedia.org). Et ici dans la salle informatique les ordinateurs ne sont pas associés à un nom de domaine.

!!! info "Ce qu'il faudrait pour réellement mettre votre site en ligne"
    Votre site est actuellement accessible seulement depuis le réseau du lycée, car l'adresse IP de l'ordinateur est une adresse locale au réseau du lycée (si vous la tapez dans le navigateur de votre téléphone, ça ne marchera pas).

    Pour mettre votre site en ligne, il faudrait le placer sur un serveur connecté à internet avec une adresse IP accessible, éventuellement louer un nom de domaine et l'y associer. Sur ce serveur un logiciel tourne en permanence pour distribuer le site (les plus connus sont apache et nginx). Typiquement, on loue de l'espace chez un hébergeur web qui propose ce service (certains sont gratuits, mais parfois avec de la pub). 

## Différents types de page web : statique ou non

Les langages de présentation HTML et CSS ont des limites si on n'utilise que ça.

On peut vouloir faire deux choses en plus :

* ne pas envoyer la même page HTML/CSS à tout le monde. Par exemple, un site sur lequel vous êtes connecté vous envoie en général une page personnalisée avec des infos qui vous sont propres. Dans ce cas, le serveur crée ou modifie le HTML pour envoyer des fichiers différents à chaque requête. Ce serveur est un logiciel codé dans n'importe quel langage de programmation, par exemple python.

* si par contre le site est le même pour tout le monde, on parle de site **statique**. Mais on peut vouloir ajouter du code qui sera exécuté sur l'ordinateur du client pour rajouter par exemple de l'interactivité : c'est le cas de tous les sites où vous pouvez faire quelque chose en cliquant sans que ça charge une nouvelle page. Dans ce cas il faut un vrai langage de programmation en plus de HTML/CSS dans le site, qui sera exécuté par le navigateur. Ce langage est javascript.

Les deux options sont bien sûr combinables : on peut avoir à la fois une page dynamique (exécution de code côté serveur) avec du javascript dedans (exécution de code côté client).
