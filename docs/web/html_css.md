# Les langages HTML et CSS

HTML et CSS sont les langages à la base de toutes les pages web.

* HTML permet d'écrire et structurer le **contenu** d'une page.
* CSS permet d'écrire des règles de **style** pour contrôler l'aspect de la page.

Ce ne sont pas des langages de programmation, car on ne peut pas faire de calculs, de boucles, de fonctions... Si on veut rajouter ces choses dans une page web il faudra faire appel au langage **javascript**.

Les **navigateurs** (Firefox, Chrome, Safari...) lisent les fichiers html, css, mais aussi images et vidéos qui composent un site et fabriquent l'affichage de la page sur l'écran.

Vous pouvez voir le code HTML de n'importe quelle page que vous visitez avec un clic droit et "Code source de la page" sur Firefox. Vous pouvez aussi ouvrir la console de développement avec F12 pour avoir encore plus d'informations.

## HTML

C'est un langage où on place le contenu dans des **balises** imbriquées les unes dans les autres.

Voir l'[introduction à HTML](https://nodfs.xyz/web/html.html) pour apprendre les bases.

## CSS

C'est un langage sous forme de **règles** qui s'appliqueront à du code HTML.

Voir l'[introduction à CSS](https://nodfs.xyz/web/css.html) pour apprendre les bases.

## Travail à faire

Créez un petit site web sur le sujet de votre choix, avec :

* au moins deux pages reliées entre elles
* des titres, images, liens vers d'autres sites web
* plusieurs règles CSS dans un fichier à part pour modifier le style

Vous pouvez écrire le html/css avec n'importe quel éditeur de texte : Geany ou VSCodium au lycée, Notepad++ par exemple sur windows mais même le bloc-note fonctionne. Il est pratique d'avoir au moins la coloration syntaxique.

Pour visualiser votre site, il suffit d'ouvrir le fichier html principal dans un navigateur.

Mettez tous vos fichiers (html, css, images) dans un dossier sur [ce lien nextcloud](https://nuage03.apps.education.fr/index.php/s/cEPwGdBck9rAbsL) pour pouvoir y accéder chez vous et les rendre.