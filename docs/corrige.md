# Devoir de NSI -- Correction

## Exercice 1 (6.5 pts)

1. **(1.5 pts)** Pour chaque code HTML suivant, dire s'il est correct **pour les règles d'imbrication des balises**. S'il est faux, expliquer le problème.
	<!-- un ok, un balise pas fermée, un pb imbrication-->
	1. `<div> <p> <h1> </h1> </div>`

        > Incorrect car la balise `<p>` n'est pas fermée.

	2. `<body> <ul> <li> </li> <li> </li> </ul> </body>`

        > Correct

	3. `<h1> <p> </p> <div> </h1> </div>`

        > Incorrect pour l'imbrication des balises entre h1 et div. Deux balises doivent être l'une dans l'autre, ou l'une après l'autre, mais pas en chevauchement comme ça.

2. **(1pt)** Expliquer le rôle du HTML dans une page web, puis du CSS dans une page web (une phrase maximum pour chaque).

    > Le HTML sert à écrire le contenu structuré d'une page web, le CSS sert à ajouter des règles de style qui changeront l'apparence de la page.

3. **(2 pts)** Écrivez les 10 premiers nombres binaires à partir de 0 (qui correspondent donc aux nombres de 0 à 9 en base 10).

    > 0,1,10,11,100,101,110,111,1000,1001

4. **(2 pts)** Écrivez le code d'une fonction python `max` qui prend deux paramètres de type int et renvoie le plus grand des deux.

    ```python
    def max(a, b):
        if a > b:
            return a
        else:
            return b
    ```

## Exercice 2 : Programmation (5.5 pts)

On veut programmer un jeu de plate-forme à une dimension : un niveau est
composé d'une suite de cases qui peuvent être vides ou
contenir un obstacle de hauteur 1 ou 2, par dessus lesquels le personnage
devra sauter.  
On représente un niveau en python par une liste d'entiers, un par case,
0 si elle est vide et 1 ou 2 selon la hauteur de l'obstacle sinon.
Par exemple le niveau suivant:
```console
  #
_##_#_
```
est représenté par la liste python `[0,1,2,0,1,0]`.

**question 1 (1 pt):** Si la variable `niveau` contient une liste, écrivez les instructions :

* qui affiche la première valeur de la liste `niveau`
  > `print(niveau[0])`

* qui affiche la dernière valeur de la liste `niveau` (il faudra utiliser la fonction `len`)
 > `print(niveau[len(niveau)-1])`
 > Ou bien en créant une variable intermédiaire pour la taille de la liste:
	```python
	n = len(niveau)
	print(niveau[n-1])
	```

**question 2 (2.5 pts):** On exécute le programme suivant:
```python
niveau = [1,0,2,1,0,1]
nombre_obstacles = 0
nombre_cases = len(niveau)
for i in range(nombre_cases):
	if niveau[i] > 0:
		nombre_obstacles = nombre_obstacles + 1
```
Recopiez et complétez le tableau de la valeur des variables à chaque tour de la boucle for. Ajoutez autant de lignes que nécessaire jusqu'à la fin de la boucle.

|i|niveau[i]|nombre_obstacles|
|---|---|---|
|0|1|1|
|1|0|1|
|2|2|2|
|3|1|3|
|4|0|3|
|5|1|4|

**question 3 (2 pts):** Le niveau sera possible s'il ne contient pas 4 obstacles d'affilée. On a commencé à écrire la fonction suivante pour vérifier qu'un niveau est possible:
```python
def est_possible(niveau):
	nombre_cases = len(niveau)
	for i in range(???):
		if niveau[i]>0 and niveau[i+1]>0 and niveau[i+2]>0 and niveau[i+3]>0:
			return False
```

* Que faut-il mettre à la place des `???` pour que tous les blocs de 4 cases d'affilée soient testés **et qu'on n'essaye jamais d'accéder à une case en dehors de la liste**. Justifiez.

    > Il faut mettre `nombre_cases - 3` pour que `niveau[i+3]` ne sorte jamais de la liste : on s'arrête trois cases avant la dernière car on vérifie à chaque fois la case et les trois suivantes.

* Il manque une dernière ligne avec `return True` qui doit s'exécuter quand le niveau est possible. On hésite sur le décalage à droite entre :
	* au niveau du def (tout à gauche)
	* au niveau du for (une fois la touche TAB)
	* au niveau du if (deux fois la touche TAB)

	Pour chacun des trois cas, expliquer ce qu'il se passerait.

    > * Au niveau du def on aurait une erreur car le return serait en dehors de la fonction, ce qui est interdit.
    > * Au niveau du for c'est correct : on ne renvoie True qu'à la fin de la boucle for, si tous les tests pour trouver 4 obstacles consécutifs ont échoués.
    > * Au niveau du if, cela s'arrêterait après le premier test et renverrait True immédiatement **sans vérifier la suite**. Par exemple sur la liste `[0,1,1,1,1]` le résultat serait incorrect.

## Exercice 3 : Algorithmes de Tri (3 pts)

On rappelle que :

* le tri par sélection _sélectionne_ le minimum dans la partie qu'il reste à trier et l'échange avec celui au début de cette partie.
* le tri par insertion prend chaque nombre dans l'ordre, et l'_insère_ dans les nombres précédents qui sont déjà triés.

Recopiez et complétez le tableau suivant avec chaque étape du tri, pour les deux méthodes jusqu'à ce que les nombres soient triés.

|étape|tri par sélection|tri par insertion|
|---|---|---|
|0|7 3 4 1 0 5|7 3 4 1 0 5|
|1|**0** 3 4 1 **7** 5|**3** 7 4 1 0 5|
|2| 0 **1** 4 **3** 7 5 | 3 **4** 7 1 0 5|
|3| 0 1 **3** **4** 7 5 | **1** 3 4 7 0 5|
|4| 0 1 3 **4** 7 5 | **0** 1 3 4 7 5|
|5| 0 1 3 4 **5** **7** | 0 1 3 4 **5** 7|
|6| 0 1 3 4 5 **7**||

> Pour le tri par insertion, on a montré juste le résultat de l'insertion de chaque nombre successif de la liste dans le début de la liste. On pourrait détailler en montrant chaque étape par lequel un nombre est décalé vers la gauche jusqu'à être à sa place.

## Exercice 4 : Problème d'Algorithmique (5 pts)

Suite à une réforme, il n'y a plus de sonneries au lycée l'an prochain et tous les professeurs peuvent choisir librement les horaires de leurs cours (par exemple de 9h42 à 11h14).  
Vous connaissez les horaires de chaque cours, et comme vous adorez apprendre vous voulez trouver le plus grand nombre de cours que vous pouvez suivre. La seule règle est que vous ne pouvez pas suivre deux cours qui ont lieu en même temps.  
On va travailler comme exemple avec la liste de cours suivante :

* Latin: 8h04 -> 9h38
* Histoire: 9h20 -> 10h15
* Maths: 10h02 -> 15h14
* EPS: 12h -> 16h
* Anglais: 15h55 -> 17h

**question 1:** On utilise l'algorithme suivant: on ajoute à chaque fois le cours **qui a la durée la plus courte** parmi les cours compatibles avec ceux qu'on a déjà pris, jusqu'à ce qu'on ne puisse plus en rajouter. Donner les cours qu'on obtient, dans l'ordre où on les a ajoutés.  
??? check "Solution"
	Histoire et Anglais.
**question 2:** Même question en prenant à chaque fois le cours **qui commence le plus tôt**.  
??? check "Solution"
	Latin, Maths, Anglais.
**question 3:** Même question en prenant à chaque fois le cours **qui termine le plus tôt**.  
??? check "Solution"
	Latin, Maths, Anglais.
**question 4:** On se demande si ces algorithmes sont optimaux, c'est à dire si ils donnent toujours le plus grand nombre de cours possibles. Est-ce que les question 1,2,3 permettent de conclure sur certains d'entre eux ?  
??? check "Solution"
	Juste avec cet exemple on ne **peut pas** déduire qu'un des algorithmes est toujours mieux qu'un autre, ni que l'un d'entre eux est optimal (c'est à dire le toujours le meilleur possible). La seule chose qu'on peut déduire c'est que l'algorithme 1 n'est **pas** optimal parce qu'il donne une solution moins bonne que les autres, donc non optimale, sur un exemple. Il ne peut donc pas être le meilleur tout le temps.
**question 5:** Si deux des algorithmes étaient à égalité dans les questions 1,2,3, trouvez une liste de cours où un algorithme est meilleur que l'autre.  
??? check "Solution"
	Si on a:
	
	* Maths: 8h -> 17h
	* Anglais: 9h -> 10h
	* Français: 11h -> 12h
	
	Prendre à chaque fois le cours qui commence le plus tôt donne juste Maths, alors que prendre celui qui finit le plus tôt donne Anglais et Français. On en déduit que la méthode de la question 2 n'est pas optimale.
