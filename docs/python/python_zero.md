# Tout ce qu'on a le droit d'écrire en python

Un programme en python est une suite d'instructions, une sur chaque ligne.

Sauf instructions spéciales, python exécute les lignes une par une dans l'ordre.

Quelles sont les instructions possibles ? On peut les regrouper en **trois** catégories :

## 1) Les expressions

Une expression est quelque chose qu'on peut **évaluer** et remplacer par une **valeur**
(et les valeurs ont toujours un **type**).

Voici les expressions possibles :

### A. les constantes

Une constante est déjà une valeur, il n'y a donc pas besoin de l'évaluer. Par exemple :

* `42` : c'est un nombre entier, son type est **int**
* `3.14` : c'est un nombre "à virgule flottante", son type est **float**
* `"Bonjour !"` : c'est une chaîne de caractère, son type est **string**
* `True` : c'est un booléen (comme `False`), son type est **bool**
* `[1,42,"blabla"]` : c'est une liste (on dit aussi tableau), son type est **list**
* `None` : est une valeur spéciale de python quand on ne veut pas mettre de valeur, son type est **NoneType**

### B. les variables

Une variable qui a déjà été définie avant dans le programme peut être utilisée comme
une expression. L'évaluation remplacera la variable par la valeur vers laquelle elle pointe.

Par exemple si on a déjà écrit "a = 3" avant dans le programme, `a` est une expression
valide qui une fois évaluée sera remplacée par la valeur `3`.

### C. les expressions numériques (calculs)

Les opérateurs de calcul `+ - * / // % **` permettent de combiner une ou plusieurs
expressions.

Par exemple si `expression1` et `expression2` sont des expressions, 
`expression1 + expression2` est une expression. Pour l'évaluer, python évalue `expression1`
qui donne une valeur `valeur1` et expression2 qui donne `valeur2` et ensuite calcule
`valeur1 + valeur2` qui donne la valeur finale. Ce n'est possible que si `valeur1` et
`valeur2` ont des types qu'on a le droit d'additionner ensemble.

### D. les expressions logiques (qui s'évaluent en True et False)

Les opérateurs de comparaison et d'égalité `> < >= <= == !=` permettent
de combiner plusieurs expressions et leur évaluation donne une valeur
de type bool : True ou False.

Les opérateurs logiques `and or not` permettent de combiner des expressions
dont les valeurs sont de type bool pour donner une autre valeur de type bool.

**exemples**:

* `10 < 3` qui s'évalue en `False`
* `True or False` qui s'évalue en `True`

### E. Les appels de fonction

Si une fonction `f` est définie, et qu'elle prend par exemple un paramètre entier,
alors `f(3)` est une expression valide.

Pour l'évaluer, python remplace le paramètre dans la définition de f par 3 et exécute
le code de la fonction f. Si on atteint un `return truc` alors la valeur sera `truc`.
Si la fonction se termine sans `return` alors la valeur est `None` (une valeur spéciale).

Si vous voyez un appel de fonction étrange avec un point `.` au milieu, il sera de la forme
`variable.fonction(paramètres)`. On a vu par exemple que pour ajouter la valeur 3 à une
liste qui est dans la variable L on peut faire `L.append(3)`. Ce sont des fonctions
spéciales de la programmation orientée objet (qu'on voit en Terminale), qui servent
en général à modifier la variable devant le point.

**exemples**:

* max(10,12) qui s'évalue en `12`
* print("Bonjour") qui s'évalue en `None` (et qui en plus affiche du texte)

## 2) Les assignations

Une assignation est une instruction `variable = expression` où variable est un
nom de variable, et expression n'importe quelle expression.

Pour l'exécuter python évalue d'abord l'expression pour obtenir une valeur, 
et fait ensuite pointer la variable vers cette valeur.

Si la variable n'existait pas, elle est créée, sinon sa valeur est remplacée.

**exemples**:

* `nombre_de_tours = 5`
* `message = "Hello world !"`
* `x = x + 1`

!!! danger "Attention"
	L'assignation **n'est pas** symétrique. À gauche du signe égal on doit avoir
	un nom de variable, et à droite une expression (qui peut aussi être un nom de variable, mais pas que).


## 3) Les instructions spéciales

Les instructions spéciales commencent toujours par un mot-clé de python, ceux qu'on connaît sont :

### A. Les instructions de création de blocs: **def**, **if**, **elif**, **else**, **for**, **while**

Ces instructions ont toutes pour point commun:

* d'être sur une ligne qui se termine par deux points ":"
* d'être suivies par un **bloc**, c'est à dire une ou plusieurs lignes d'instructions qui sont toutes
  **décalées à droite d'un même nombre d'espaces**. On obtient ce décalage en général avec la touche "tab".
  
Résumé de ce à quoi elles servent :

* Créer une fonction appelée blabla, qui prend 0,1 ou plusieurs paramètres :
```python
def blabla(liste des noms des paramètres séparés par des virgules):
	bloc (code exécuté quand on appelera la fonction)
```
* Créer un bloc qui s'exécutera seulement **si** l'expression logique `expression_logique` s'évalue en `True`:
```python
if expression_logique:
	bloc
```
* Juste après le bloc d'un if, on peut créer un ou plusieurs blocs **elif** qui s'exécutent si les conditions
précédentes étaient fausses mais la nouvelle est vraie:
```python
elif expression_logique:
	bloc
```
* Juste après un if (et éventuellement un ou plusieurs elif), **else** crée un bloc qui s'exécute si toutes les
conditions précédentes étaient fausses:
```python
else:
	bloc
```
* Créer un bloc qui s'exécute autant de fois qu'il y a d'éléments dans `itérateur` et où `variable` vaut successivement
toutes les valeurs qui sont dans `itérateur`:
```python
for variable in itérateur:
	bloc
```
  On utilise souvent `range(nombre)` comme itérateur qui contient les valeurs de 0 jusque `nombre-1`.
* Créer un bloc qui se ré-exécute tant que l'expression logique `expression_logique` s'évalue en `True`. Cette
  expression est ré-évaluée à chaque tour de la boucle.
```python
while expression_logique:
	bloc
```

### B. Les autres instructions spéciales: **return**, **import**, **from**, **assert**, **pass**

* `return expression` peut s'utiliser seulement dans un bloc de définition de fonction.
  Lorsque cette instruction est exécutée, `expression` est évaluée, la fonction **est arrêtée immédiatement**
  et l'appel de la fonction est remplacée par la valeur obtenue.
* **import** permet de rendre disponibles des fonctions pythons dont le code est dans d'autres
  fichiers (qu'on appelle modules ou plus généralement bibliothèques). Il y a deux formes:
    - `import math` importe tout le module math mais il faut utiliser `math.sin` pour accéder à la fonction
	`sin` du module.
	- `from math import sin,cos` permet d'importer certaines fonctions qu'on pourra appeler directement.
* `assert expression_logique` évalue `expression_logique` et déclenche une erreur si la valeur n'est pas `True`.
  On l'utilise pour tester des fonction ou dans le code pour qu'on soit prévenu par une erreur s'il y a
  une condition qu'on pense vraie mais on n'est pas sûr de n'avoir pas fait d'erreur.
* `pass` doit être mis si on crée un bloc vide (il faut au moins une instruction alors on met celle là, qui ne fait rien).


---

On a fait le tour ! Maintenant, vous savez tout et vous pouvez dire quand une ligne n'est pas correcte,
ou bien qu'elle utilise d'autres fonctionnalités de python qu'on n'a pas étudiées.



