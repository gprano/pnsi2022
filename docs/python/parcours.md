# Parcours de listes et dictionnaires

## Listes (= tableau)

Une liste est créée par : `[3,54,"hello",False]` (mais en général on ne met qu'un seul type dedans)

Ou bien pour répéter une même valeur : `[0] * 100` (liste de 100 zéros)

Ou bien par compréhension : `[2*i for i in range(1,11)]` (nombres pairs de 2 à 20)

### Parcours par indice

Si la liste est stockée dans une variable `maliste`, on accède à l'élément d'**indice** n°`i` avec `maliste[i]`.

On peut parcourir tous les indices possibles de `0` à `len(maliste)-1` grâce à `range(len(maliste))` :

```python
for i in range(len(maliste)):
    # i est l'indice en cours, et maliste[i] la valeur à cet indice dans maliste
```

### Parcours par valeur

On peut aussi parcourir directement les valeurs :

```python
for v in maliste:
    # v prend successivement chaque valeur de la liste
    # on ne peut PAS accéder à l'indice
```

## Dictionnaires (= tableau associatif)

Un dictionnaire est créé par : `{"nom" : "Durand", "age" : 12}`

On peut aussi le créer par compréhension : `{i : i**2 for i in range(100)}` (associe à chaque nombre son carré)

Les éléments sont indexés par les clés, donc dans le premier cas `mondico["nom"]` vaut `Durand`.

### Parcours par indice

C'est impossible : un dictionnaire n'est pas ordonné, il n'y a donc pas d'élément 0, 1, etc.

### Parcours des clés

La manière de parcourir un dictionnaire est de parcourir toutes ses **clés**, ce qu'on peut faire avec :

```python
for cle in mondico:
    # ici cle vaut successivement chaque clé, et si on veut la valeur associée on utilise mondico[cle]
```

On peut aussi utiliser (c'est exactement pareil mais c'est explicite) la méthode .keys() : 

```python
for cle in mondico.keys():
    # idem
```

!!! hint "Tester si une clé est présente dans le dictionnaire"
    L'expression `cle in mondico` vaut True si `cle` est une clé dans `mondico`, False sinon.

    Contrairement à l'expression `valeur in maliste` qui doit parcourir toute la liste pour savoir si la valeur est dedans, python a une méthode très efficace pour savoir si une clé est dans un dictionnaire qui ne dépend pas de la taille du dictionnaire.

!!! hint "Remplir un dictionnaire vide"
    Il arrive qu'on veuille modifier une valeur dans le dictionnaire si la clé y est déjà, et l'ajouter si elle n'y est pas, par exemple si on veut compter le nombre d'apparition de chaque lettre dans une phrase :

    ```python
    if lettre in mondico:
        mondico[lettre] = mondico[lettre] + 1
    else:
        mondico[lettre] = 1
    ```

    L'instruction dans le `if` provoquerait une erreur si `lettre` n'était pas dans le dictionnaire à cause de l'expression à droite du `=` dans l'assignation.



