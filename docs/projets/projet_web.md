# Miniprojet de site web

Vous allez créer votre propre petit site internet : un ensemble de fichiers html et css (éventuellement javascript), d'images, le tout dans un même dossier et qui lorsqu'on l'ouvre avec un navigateur web affiche le site.

Vous pourrez vous aider du [site d'aide](https://nodfs.xyz/web/) que j'ai créé pour un projet de SNT.

Le choix du sujet du site n'est pas complètement libre, vous devez faire une petite page sur un sujet précis en lien avec l'informatique. Comme un petit exposé, mais sous forme d'une (ou plusieurs) pages web.

## Exemples de sujets

* Un personnage de l'histoire de l'informatique comme :
    - Ada Lovelace
    - Alan Turing
	- Grace Hopper
* L'histoire de la création d'une technologie comme :
    - le web
	- le GPS
	- le bitcoin
* Une explication d'une fonctionnalité de python comme :
    - les f-string
	- les fonctions anonymes avec `lambda`
* Un langage de programmation :
    - C
	- Javascript
* Un tutoriel avec des images pour expliquer quelque chose que vous savez faire sur un ordinateur.
* L'impact sur l'environnement des technologies de l'informatique.
* Tout autre sujet qui vous intéresse, demandez-moi pour que je le valide.

## Points importants

* Vous devez m'indiquer en commentaires `<!-- ceci est un commentaire en html -->` vos **sources** si vous réutiliser du code css,html ou javascript trouvé sur internet, ou si vous réutilisez du contenu pour votre page.
* les images que vous utilisez doivent être sous une licence qui autorise la réutilisation (Creative Commons par exemple).

